export default {
  no: 0,
  contacts: [
    { no: 1001, name: '김유신', tel: '010-1212-3331', address: '경주' },
    { no: 1002, name: '장보고', tel: '010-1212-3332', address: '청해진' },
    { no: 1003, name: '관창', tel: '010-1212-3333', address: '황산벌' },
    { no: 1004, name: '안중근', tel: '010-1212-3334', address: '해주' }
  ]
}
